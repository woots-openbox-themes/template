======================
Openbox theme template
======================

This is simply a template with all customizable properties and lots of
comments from the `official site`_ in order to understand what they do.

.. _official site: http://openbox.org/wiki/Help:Themes
